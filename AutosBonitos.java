
package Modelo;

public class AutosBonitos {
    int idDueño;
    String placa;
    String numero;
    String color;

    public AutosBonitos() {
    }

    public AutosBonitos(int idDueño, String placa, String numero, String color) {
        this.idDueño = idDueño;
        this.placa = placa;
        this.numero = numero;
        this.placa = placa;
    }

    public int getidDueño() {
        return idDueño;
    }

    public void setId(int idDueño) {
        this.idDueño = idDueño;
    }

    public String getplaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getnumero() {
        return numero;
    }

    public void setnumero(String numero) {
        this.numero = numero;
    }

    public String getcolor() {
        return color;
    }

    public void setcolor(String color) {
        this.color = color;
    }
    
    
}
