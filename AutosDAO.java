package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AutosDAO {

    PreparedStatement ps;
    ResultSet rs;
    Connection con;
    Conexion conectar = new Conexion();
    AutosBonitos p = new AutosBonitos();

    public List listar() {
        List<AutosBonitos> datos = new ArrayList<>();
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement("select * from ejemplo");
            rs = ps.executeQuery();
            while (rs.next()) {
                AutosBonitos p = new AutosBonitos();
                p.setId(rs.getInt(1));
                p.setPlaca(rs.getString(2));
                p.setnumero(rs.getString(3));
                p.setcolor(rs.getString(4));
                datos.add(p);
            }
        } catch (Exception e) {
        }
        return datos;
    }
    public int agregar(AutosBonitos per) {  
        int r=0;
        String sql="insert into ejemplo(Placa,Numerodeesta,Color)values(?,?,?)";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);            
            ps.setString(1,per.getplaca());
            ps.setString(2,per.getnumero());
            ps.setString(3,per.getcolor());
            r=ps.executeUpdate();    
            if(r==1){
                return 1;
            }
            else{
                return 0;
            }
        } catch (Exception e) {
        }  
        return r;
    }
    public int Actualizar(AutosBonitos per) {  
        int r=0;
        String sql="update persona set Placa=?,Numerodeesta=?,Color=? where Id=?";        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);            
            ps.setString(1,per.getplaca());
            ps.setString(2,per.getnumero());
            ps.setString(3,per.getcolor());
            ps.setInt(4,per.getidDueño());
            r=ps.executeUpdate();    
            if(r==1){
                return 1;
            }
            else{
                return 0;
            }
        } catch (Exception e) {
        }  
        return r;
    }
    public int Delete(int id){
        int r=0;
        String sql="delete from persona where Id="+id;
        try {
            con=conectar.getConnection();
            ps=con.prepareStatement(sql);
            r= ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }

   
}
